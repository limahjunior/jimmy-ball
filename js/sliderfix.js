//declaração do framework
var JB = {};
var levelteste;
var stage;
//declaração do objeto de controle do game, neste caso, os levels
JB.Level1 = "";
JB.Level2 = "";
JB.Level3 = "";
JB.Level4 = "";
JB.Level5 = "";
JB.Level6 = "";
JB.Level7 = "";
JB.Level8 = "";
JB.Level9 = "";
JB.Level10 = "";
JB.Level11 = "";
JB.Level12 = "";
JB.Level13 = "";
JB.Level14 = "";
JB.Level15 = "";
JB.Level16 = "";
JB.Level17 = "";
JB.Level18 = "";
JB.Level19 = "";
JB.Level20 = "";
//função que traz os dados do localStorage e atualiza seu objeto de controle do game
JB.Start = {};
//função chamada na inialização da página
JB.Init = {};
//declaração do banco de dados (localStorage)
JB.DataBase = {};
//funções nativas do localStorage, para instanciar o BD (Get) e alterar os valores do BD (Set)
JB.DataBase.Get = {};
JB.DataBase.Set = {};
//função criada para construir objeto JSON e armazenar no localStorage
JB.DataBase.Build = {};


JB.Init = function () {
    JB.DataBase.Build();
    JB.Start();
};

JB.Start = function () {
    /*
     devo pegar os levels no localStorage e setar os levels do meu objeto
     cada level do meu objeto foi declarado acima
     devo pegar e converter para objeto JSON para fazer modificações
     */

    //aqui eu declarei uma instância do localStorage
    var dataBase = JB.DataBase.Get();

    //aqui eu puxo a base 'jimmyBall' e converto para JSON
    var objetoLocalStorage = dataBase.getItem("jimmyBall");
    var objetoJSON = JSON.parse(objetoLocalStorage);

    /*
     aqui eu estou atribuindo o valor de cada leval trazido do localStorage nas variáveis locais de controle do game
     note que eu faço isso tratando os valores como atributos de uma classe
     */

    JB.Level1 = objetoJSON.level1;
    JB.Level2 = objetoJSON.level2;
    JB.Level3 = objetoJSON.level3;
    JB.Level4 = objetoJSON.level4;
    JB.Level5 = objetoJSON.level5;
    JB.Level6 = objetoJSON.level6;
    JB.Level7 = objetoJSON.level7;
    JB.Level8 = objetoJSON.level8;
    JB.Level9 = objetoJSON.level9;
    JB.Level10 = objetoJSON.level10;
    JB.Level11 = objetoJSON.level11;
    JB.Level12 = objetoJSON.level12;
    JB.Level12 = objetoJSON.level12;
    JB.Level13 = objetoJSON.level13;
    JB.Level14 = objetoJSON.level14;
    JB.Level15 = objetoJSON.level15;
    JB.Level16 = objetoJSON.level16;
    JB.Level17 = objetoJSON.level17;
    JB.Level18 = objetoJSON.level18;
    JB.Level19 = objetoJSON.level19;
    JB.Level20 = objetoJSON.level20;

    //a partir daqui, eu tenho todos os levels do meu objeto instanciados e atribuídos.

};

//aqui eu vou verificar se o browser suporta o uso do localStorage, se sim, eu devolvo uma instância do localStorage. Se não, eu devolvo 'undefined'.
JB.DataBase.Get = function () {
    try {
        if (window.localStorage)
            return window.localStorage;
    } catch (e) {
        return undefined;
    }
};

//o parâmetro 'objeto' recebido é do tipo JSON
JB.DataBase.Set = function (objeto) {
    var objTexto;//variável 'string' que irá converter um objeto JSON para texto
    var nomeBancoDeDados;//variável 'string' que irá determinar o nome do banco de dados no localStorage

    nomeBancoDeDados = "jimmyBall";
    objTexto = JSON.stringify(objeto);

    JB.DataBase.Get().setItem(nomeBancoDeDados, objTexto);
};

JB.DataBase.Build = function () {
    /*
     verificar se o dataBase já existe no localStorage com o nome determinado.
     se não existir, irei construir o banco de dados.
     se já existir, faço nada
     */

    var dataBase = JB.DataBase.Get();

    if (dataBase.getItem("jimmyBall") === null) {

        /*
         se o banco de dados não existir, irei criar um banco de dados no localStorage com chave 'jimmyBall' e no valor irei jogar um objeto JSON com 5 levels.
         somente o primeiro level estará liberado, ou seja, setado como 'true. os demais estarão bloqueados e setados como 'false'
         */
        dataBase.setItem("jimmyBall",
                '{\
                "level1": "true",\
                "level2": "false",\
                "level3": "false",\
                "level4": "false",\
                "level5": "false",\
                "level6": "false",\
                "level7": "false",\
                "level8": "false",\
                "level9": "false",\
                "level10": "false",\
                "level11": "false",\
                "level12": "false",\
                "level13": "false",\
                "level14": "false",\
                "level15": "false",\
                "level16": "false",\
                "level17": "false",\
                "level18": "false",\
                "level19": "false",\
                "level20": "false"\
            }');
    }
};

$(document).ready(function () {
    JB.Init();
    $("#levels").hide();
});

var clear = function () {
    $("#startscreen").hide(); // ---> Start Screen
    $("#splashgame").hide(); // ---> Splash Screen
    $("#splashscreen").hide(); // ---> Splash Screen
    $("#stages").hide(); // ---> Carousel Stages
    $("#levels").hide(); // ---> Carousel Levels
};

var clearAudio = function () {
    back.pause();
    music1.pause();
    music2.pause();
    music3.pause();
    music4.pause();
    music5.pause();
    music6.pause();
    music7.pause();
    music8.pause();
    music9.pause();
    music10.pause();
    music11.pause();
    music11.pause();
    music12.pause();
    music13.pause();
    music14.pause();
    music15.pause();
    music16.pause();
    music17.pause();
    music18.pause();
    music19.pause();
    music20.pause();
};

$(document).ready(function () {
    $("#levels").hide();
    $("#stages").show();
});

//Stages

$("#btnLevel01").click(function () {
    sound.play();
    $("#stages").hide();
    $("#levels").show();
    stage = 1;
});

$("#btnLevel02").click(function () {
    if (JB.Level11 === "false") {
        alert("Mason Não Liberado");
    } else if (JB.Level11 === "true") {
        sound.play();
        $("#stages").hide();
        $("#levels").show();
        stage = 2;
    }
});

// Levels

$("#level-select-1").click(function () {
    if (stage === 1) {
        sound.play();
        alert("Level 1");
        speed = 2.5;

        clear();
        $.get("game1.html", function (data) { // ---> Get take the file html and put in a function with data param
            $("#gamescreen").append(data); // ---> Append show gamescreen
        });

        levelteste = JB.Level2;
    } else if (stage === 2) {
        sound.play();
        alert("Level 11");
        speed = 2.5;

        clear();
        $.get("game11.html", function (data) { // ---> Get take the file html and put in a function with data param
            $("#gamescreen").append(data); // ---> Append show gamescreen
        });

        levelteste = JB.Level12;
    }

});

$("#level-select-2").click(function () {
    if (stage === 1) {
        if (JB.Level2 === "false") {
            alert("2 Não Liberado");
        } else if (JB.Level2 === "true") {
            sound.play();
            alert("Level 2");
            speed = 3;

            clear();
            $.get("game2.html", function (data) { // ---> Get take the file html and put in a function with data param
                $("#gamescreen").append(data); // ---> Append show gamescreen
            });




            levelteste = JB.Level3;
        }
    } else if (stage === 2) {
        if (JB.Level12 === "false") {
            alert("12 Não Liberado");
        } else if (JB.Level2 === "true") {
            sound.play();
            alert("Level 12");
            speed = 3;

            clear();
            $.get("game12.html", function (data) { // ---> Get take the file html and put in a function with data param
                $("#gamescreen").append(data); // ---> Append show gamescreen
            });

            levelteste = JB.Level13;
        }
    }
});



$("#level-select-3").click(function () {
    if (stage === 1) {
        if (JB.Level3 === "false") {
            alert("3 Não Liberado");
        } else if (JB.Level3 === "true") {
            sound.play();
            alert("Level 3");
            speed = 3.5;

            clear();
            $.get("game3.html", function (data) { // ---> Get take the file html and put in a function with data param
                $("#gamescreen").append(data); // ---> Append show gamescreen
            });




            levelteste = JB.Level4;
        }
    } else if (stage === 2) {
        if (JB.Level13 === "false") {
            alert("13 Não Liberado");
        } else if (JB.Level13 === "true") {
            sound.play();
            alert("Level 13");
            speed = 3.5;

            clear();
            $.get("game13.html", function (data) { // ---> Get take the file html and put in a function with data param
                $("#gamescreen").append(data); // ---> Append show gamescreen
            });

            levelteste = JB.Level14;
        }
    }
});

$("#level-select-4").click(function () {
    if (stage === 1) {
        if (JB.Level4 === "false") {
            alert("4 Não Liberado");
        } else if (JB.Level4 === "true") {
            sound.play();
            alert("Level 4");
            speed = 4;

            clear();
            $.get("game4.html", function (data) { // ---> Get take the file html and put in a function with data param
                $("#gamescreen").append(data); // ---> Append show gamescreen
            });




            levelteste = JB.Level5;
        }
    } else if (stage === 2) {
        if (JB.Level14 === "false") {
            alert("14 Não Liberado");
        } else if (JB.Level14 === "true") {
            sound.play();
            alert("Level 14");
            speed = 4;

            clear();
            $.get("game14.html", function (data) { // ---> Get take the file html and put in a function with data param
                $("#gamescreen").append(data); // ---> Append show gamescreen
            });

            levelteste = JB.Level15;
        }
    }
});

$("#level-select-5").click(function () {
    if (stage === 1) {
        if (JB.Level5 === "false") {
            alert("5 Não Liberado");
        } else if (JB.Level5 === "true") {
            sound.play();
            alert("Level 5");
            speed = 4.5;

            clear();
            $.get("game5.html", function (data) { // ---> Get take the file html and put in a function with data param
                $("#gamescreen").append(data); // ---> Append show gamescreen
            });




            levelteste = JB.Level6;
        }
    } else if (stage === 2) {
        if (JB.Level15 === "false") {
            alert("15 Não Liberado");
        } else if (JB.Level15 === "true") {
            sound.play();
            alert("Level 15");
            speed = 4.5;

            clear();
            $.get("game15.html", function (data) { // ---> Get take the file html and put in a function with data param
                $("#gamescreen").append(data); // ---> Append show gamescreen
            });

            levelteste = JB.Level16;
        }
    }
});

$("#level-select-6").click(function () {
    if (stage === 1) {
        if (JB.Level6 === "false") {
            alert("6 Não Liberado");
        } else if (JB.Level6 === "true") {
            sound.play();
            alert("Level 6");
            speed = 5;

            clear();
            $.get("game6.html", function (data) { // ---> Get take the file html and put in a function with data param
                $("#gamescreen").append(data); // ---> Append show gamescreen
            });




            levelteste = JB.Level7;
        }
    } else if (stage === 2) {
        if (JB.Level15 === "false") {
            alert("15 Não Liberado");
        } else if (JB.Level15 === "true") {
            sound.play();
            alert("Level 15");
            speed = 5;

            clear();
            $.get("game16.html", function (data) { // ---> Get take the file html and put in a function with data param
                $("#gamescreen").append(data); // ---> Append show gamescreen
            });

            levelteste = JB.Level17;
        }
    }
});

$("#level-select-7").click(function () {
    if (stage === 1) {
        if (JB.Level7 === "false") {
            alert("7 Não Liberado");
        } else if (JB.Level7 === "true") {
            sound.play();
            alert("Level 7");
            speed = 5.5;

            clear();
            $.get("game7.html", function (data) { // ---> Get take the file html and put in a function with data param
                $("#gamescreen").append(data); // ---> Append show gamescreen
            });




            levelteste = JB.Level8;
        }
    } else if (stage === 2) {
        if (JB.Level17 === "false") {
            alert("17 Não Liberado");
        } else if (JB.Level17 === "true") {
            sound.play();
            alert("Level 17");
            speed = 5.5;

            clear();
            $.get("game17.html", function (data) { // ---> Get take the file html and put in a function with data param
                $("#gamescreen").append(data); // ---> Append show gamescreen
            });

            levelteste = JB.Level18;
        }
    }
});

$("#level-select-8").click(function () {
    if (stage === 1) {
        if (JB.Level8 === "false") {
            alert("8 Não Liberado");
        } else if (JB.Level8 === "true") {
            sound.play();
            alert("Level 8");
            speed = 6;

            clear();
            $.get("game8.html", function (data) { // ---> Get take the file html and put in a function with data param
                $("#gamescreen").append(data); // ---> Append show gamescreen
            });




            levelteste = JB.Level9;
        }
    } else if (stage === 2) {
        if (JB.Level18 === "false") {
            alert("18 Não Liberado");
        } else if (JB.Level18 === "true") {
            sound.play();
            alert("Level 18");
            speed = 6;

            clear();
            $.get("game18.html", function (data) { // ---> Get take the file html and put in a function with data param
                $("#gamescreen").append(data); // ---> Append show gamescreen
            });

            levelteste = JB.Level19;
        }
    }
});

$("#level-select-9").click(function () {
    if (stage === 1) {
        if (JB.Level9 === "false") {
            alert("9 Não Liberado");
        } else if (JB.Level9 === "true") {
            sound.play();
            alert("Level 9");
            speed = 6.5;

            clear();
            $.get("game9.html", function (data) { // ---> Get take the file html and put in a function with data param
                $("#gamescreen").append(data); // ---> Append show gamescreen
            });




            levelteste = JB.Level10;
        }
    } else if (stage === 2) {
        if (JB.Level19 === "false") {
            alert("19 Não Liberado");
        } else if (JB.Level19 === "true") {
            sound.play();
            alert("Level 19");
            speed = 6.5;

            clear();
            $.get("game19.html", function (data) { // ---> Get take the file html and put in a function with data param
                $("#gamescreen").append(data); // ---> Append show gamescreen
            });

            levelteste = JB.Level20;
        }
    }
});

$("#level-select-10").click(function () {
    
    if (stage === 1) {
        if (JB.Level10 === "false") {
        alert("10 Não Liberado");
    } else if (JB.Level10 === "true") {
        sound.play();
        alert("Level 10");
        speed = 7;

        clear();
        $.get("game10.html", function (data) { // ---> Get take the file html and put in a function with data param
            $("#gamescreen").append(data); // ---> Append show gamescreen
        });

        levelteste = JB.Level11;
    }
    } else if (stage === 2) {
        if (JB.Level20 === "false") {
            alert("20 Não Liberado");
        } else if (JB.Level20 === "true") {
            sound.play();
            alert("Level 20");
            speed = 7;

            clear();
            $.get("game20.html", function (data) { // ---> Get take the file html and put in a function with data param
                $("#gamescreen").append(data); // ---> Append show gamescreen
            });
        }
    }
});